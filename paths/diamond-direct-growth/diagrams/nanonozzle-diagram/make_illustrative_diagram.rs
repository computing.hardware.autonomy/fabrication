#!/usr/bin/env -S cargo +nightly -Zscript
---cargo
[package]
edition = "2021"

[dependencies]
anyhow = "1"
thiserror = "1"
---

use std::{
    env::args,
    fs, path,
    process::{Command, ExitCode, ExitStatus},
};

/// For rendering of the curv file.
pub mod render {
    use std::{ffi::OsStr, path, process::Command};

    pub const CURV_FILE_NAME: &str = "./basic-nanonozzle-for-image-render.curv";

    /*
    pub const XSIZE: usize = 2048;
    pub const YSIZE: usize = 1024 + 512;
    */
    // Note that experimentally, on my device, the constraints for "maximum renderable dimensions"
    // act extremely bizarre and unpredictably, to the point of sometimes smaller dimensions not
    // working whereas larger ones do.
    //
    // So we pick arbitrary and somewhat large-ish dimensions that experimentally work.
    pub const XSIZE: usize = 1000;
    pub const YSIZE: usize = 1000;

    #[derive(Debug, thiserror::Error, Clone)]
    pub enum InvalidOutputFileError {
        #[error("Invalid output file '{filename}' (wrong format suffix, not png)")]
        WrongFormat { filename: path::PathBuf },
    }

    // TODO: make some kind of trait for curv outputs that can specify format directly OR check
    // filename for automated validation nya
    pub const FORMAT_EXTENSION: &str = "png";

    #[derive(Debug, thiserror::Error)]
    pub enum RenderCommandError {
        #[error(transparent)]
        Io(#[from] std::io::Error),
        #[error(transparent)]
        OutputFile(#[from] InvalidOutputFileError),
    }

    #[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Hash, Debug)]
    enum OutputDimension {
        X,
        Y,
    }

    impl OutputDimension {
        /// Add an argument specifying the given size to the curv CLI. This typically involves two
        /// "concrete" CLI args
        pub fn add_size_arg<'c>(&self, size: usize, cmd: &'c mut Command) -> &'c mut Command {
            let cmd = cmd.arg("-O");
            match self {
                OutputDimension::X => cmd.arg(&format!("xsize={size}")),
                OutputDimension::Y => cmd.arg(&format!("ysize={size}")),
            }
        }
    }

    /// Build the render command (with args) from the given output filename, with validation
    pub fn build_render_command(
        output_filename: impl AsRef<path::Path>,
    ) -> Result<std::process::Command, InvalidOutputFileError> {
        let output_filename = output_filename.as_ref();
        if output_filename.extension() != Some(OsStr::new(FORMAT_EXTENSION)) {
            return Err(InvalidOutputFileError::WrongFormat {
                filename: output_filename.to_owned(),
            });
        }
        let mut cmd = Command::new("curv");
        cmd.arg("-v");
        // The ordering is important - output format is derived from `-o`,
        // this enables specifying options that are output-format-specific (e.g. width/height for
        // images)
        // then the curv filename to generate from is last nya
        cmd.arg("-o").arg(output_filename);
        OutputDimension::X.add_size_arg(XSIZE, &mut cmd);
        OutputDimension::Y.add_size_arg(YSIZE, &mut cmd);
        cmd.arg(CURV_FILE_NAME);
        Ok(cmd)
    }

    /// Build and start the render command
    pub fn start_render_command(
        output_filename: impl AsRef<path::Path>,
    ) -> Result<std::process::Child, RenderCommandError> {
        let output_filename = output_filename.as_ref();
        let mut cmd = build_render_command(output_filename)?;
        Ok(cmd.spawn()?)
    }
}

/// Attempt to strip the metadata from the given path by calling out to `mat2`
///
/// In case of failure to start, a warning will be printed (rather than the program dying nya)
pub fn strip_metadata(path: impl AsRef<path::Path>) -> std::io::Result<Option<ExitStatus>> {
    let mut child = match Command::new("mat2")
        .arg("--inplace")
        .arg(path.as_ref())
        .spawn()
    {
        Ok(child) => child,
        Err(e) => {
            eprintln!("[WARN] could not start `mat2` to remove png metadata: {e}");
            return Ok(None);
        }
    };
    child.wait().map(Some)
}

fn base_output_dir() -> &'static path::Path {
    path::Path::new("./output")
}

/// Make the output directory for the given output file path specified on the CLI
///
/// This merges with the "default" output dir, extracts all the directory components for that path,
/// and creates them if they do not exist.
fn make_output_directory(
    passed_output_file: impl AsRef<path::Path>,
) -> std::io::Result<std::path::PathBuf> {
    let full_output_filepath = base_output_dir().join(passed_output_file);
    // if it has any directory components that need an attempt at creation...
    if full_output_filepath.components().count() > 1 {
        let without_last = {
            let mut v = full_output_filepath.clone();
            v.pop();
            v
        };

        fs::create_dir_all(&without_last)?;
    }
    Ok(full_output_filepath)
}

fn main() -> Result<ExitCode, anyhow::Error> {
    // Skip the program name and extract the output name, which will be joined into the `output`
    // dir
    let png_output = args()
        .skip(1)
        .next()
        .unwrap_or_else(|| "diagram.png".to_owned());

    let png_output = make_output_directory(&png_output)?;

    let render_status = render::start_render_command(&png_output)?.wait()?;

    if !render_status.success() {
        anyhow::bail!("Curv failed to render diagram...");
    }

    if let Some(metadata_strip_status) = strip_metadata(&png_output)? {
        if !metadata_strip_status.success() {
            anyhow::bail!("Failed to strip metadata from output")
        }
    }

    Ok(ExitCode::SUCCESS)
}

// License: GPLv3+
