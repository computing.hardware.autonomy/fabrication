# Nanonozzle Diagram
This is meant to illustrate a rough structure via cross-section of the nanonozzles in a construction array, with the associated environment (diamond substrate, surrounding nozzles) included for better explanatory components. 

May include some information on ionised gas transport and such too. 

To view the basic nanonozzle illustrative diagram with cross-section, use `curv basic-nanonozzle.curv`. 
