# Diamond Direct-Growth Technology Path
This is a semiconductor path based on directly growing diamond and diamond-like compounds for carbon- and dopant- containing gas. This builds on ideas involved in existing chemical vapour deposition processes for thin-film diamonds.

The author considers this by far the most promising potential route. This is because of various things like the ease of access to the chemicals needed, the fact that the growth process is strictly additive (and furthermore, it's uniform - most potential processes are the same for both production of bulk material and circuitry). None of the "fine detail" components would be vitamins, and the building process for complex components would be absurdly fast as long as a 2D growth control plane is used. Not only this, but most of the processes are not limited by optics and diffraction. And, the proposed mechanisms would allow for sub-structural resolution (that is, higher resolution can be produced than what the original printer was made with). 

The main disadvantage is the fact it would be a novel semiconductor process and compared to silicon it's much less researched. And the core process the author is currently focused on is quite obscure and the exact parameters have only been simulated by obscure science papers from the '90s and 2000s. 

Modelling and simulation for fine-tuning of parameters and designs is another aspect of this that would be necessary.

More information is available on the Nanoassembler page of the RepRap Wiki.

## Papers (for records and future analysis):
* <https://www.sciencedirect.com/science/article/abs/pii/S0039602899003775> - Hydrogen abstraction from diamond surfaces in a uniform electric field - has both a weaker-field (radical pathway) and stronger field (ionic pathway) mechanism. These are ultimately all statistical processes, and we'd only need to reduce the energy barrier to where ~ room temperature has a decentish probability of induced disassociation via simple thermal noise. 
