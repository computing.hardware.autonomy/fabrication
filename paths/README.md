# PATHS
This folder contains information specific to various [paths][rrwiki-semiconductor-paths] for semiconductor fabrication.

[rrwiki-semiconductor-paths]: https://reprap.org/wiki/Nanoassembler#Developing_Technology_to_Actually_Do_This
