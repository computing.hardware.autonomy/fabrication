# Traditional Silicon Path
This is the path for generally trying to replicate the mechanisms used by traditional semiconductor manufacturing techniques.

The author does not believe this is likely to be successful in making comparable computing capabilities to modern technology in a decentralized manner (not to mention issues with self-replication). This is due - in short - due to process heterogeneity, significant non-additive processes, the need for toxic and hyperflammable chemicals (e.g. silane), and general extreme conditions. More details can be found on the RepRap wiki Nanoassembler page.
