# Plastic-Graphene Path
This path is one based on the fact that there are certain forms of plastic that can be converted to graphene in a highly controlled manner.

This has some advantages in the context of current 3D printing tech, and the fact that some of the relevant plastics can be constructed via fluid thin-film chemical mechanisms. However, it has significant drawbacks (e.g. difficulties with resolution limits, a question of reliable doping, having several second- or third- non-additive processes, etc., extreme slowness due to the need to use point writing, etc.). 

More details can be found on the Nanoassembler RepRap page. 
