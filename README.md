# computing.hardware.autonomy - Fabrication
This is a repository of information on trying to create truly decentralized CPU/semiconductor manufacturing technologies, as well as a bootstrap path from existing 3D printing tech if at all possible. From now on, this will be referred to as "Project Substrate". Code related to modelling these devices for parameter tuning will be written in future, in the [Modelling repository][modelling-repo][^future-modelling-repo-notes]

Right now, the main files involved are written using [curv][curv]. This is a language that uses Functional Representation (with Signed Distance Fields). One of the major benefits to this is that it has good support for 3-dimensional colour, which is different from something like OpenSCAD. The colour is necessary for things like representing materials and/or doping and/or voltages, plus other things.

However, `curv` is not super maintained (no shade, it doesn't seem to have caught on unfortunately and there's basically two maintainers that have worked on their lonesome for years), even if it works fine. The problem is that in the short to medium term, we want to be able to run actual simulations using the F-Rep defined structures to calculate electrodynamic behaviour and tune properties. The library `curv` uses for F-Rep management - [libfive][libfive] - is also good, but it's written in C++, and focused specifically on Signed Distance Fields. For simulation, what we need is much more general - including things like encoded derivatives/jacobians, and any other useful maths functions - plus the ability to add other fields (like how `curv` adds colour, but more general e.g. different materials, or conductivity). 

Furthermore, such a system should attempt to be extremely analytical - that is, encode the semantic algebraic structure and put actual calculations as far off as possible - to improve the performance of simulations and also result in "blobbing" all the algebra in some final parameterised field type (function), which would allow things like LLVM to optimize extremely aggressively.

The reason we stick with curv for now is that we want basic diagrams and concepts out on the internet (in particular, the [RepRap wiki][reprap-wiki]) ASAP to prevent someone from coming up with the same ideas and patenting them. This repository is licensed according to [the overall licensing scheme for this project][licensing] (all FOSS, with some variations to allow for RepRap Wiki compatibility and using OHLv2+-S for hardware stuff as it's more designed for it than AGPLv3+). You should read that before contributing (it isn't any sort of CLA, just details on the specific licenses - if you only care that they're FOSS, contribute away as they are all FOSS).

## Notes on Directory Layout
This repository has various contents, located in different directories. Contributing to this is complex because of licensing issues with [GFDL1.2], which is used on the RepRap wiki. Details on how you need to license contributions is found [here][licensing] - this is a recommended read, as when you contribute you're agreeing that you're doing so in a way compatible with the licensing scheme (if you don't specify, then it's assumed you are licensing *under that scheme*, which is all FOSS licensing and **not** some kind of Contributor License Agreement, but you might care about specifics).

### `rrwiki-pages`
These are copies of the wikicode of some RepRap pages (in wiki code) stored in the repository. This is so that if the RepRap Wiki ever dies, the full information is still available. Note that while we try to keep these up-to-date, it's not a massive priority. If you see that they are out of date, then feel free to make a merge request to update them. 

The wiki files are also licensed under the respective license on the RepRap wiki page, rather than the AGPLv3+ & OHLv2+-S (see: [licensing]). This is usually [GFDL1.2] - which is the site licnese - plus often some form of Creative Commons (in the case of the [Nanoassembler page][reprap-wiki-nanoassembler], [CC BY-SA 4.0][cc-by-sa-4.0]). Sometimes they might also be dual-licensed with other things. 

Usually pages include a small bit at the bottom that indicates the license. 

The main pages we back up right now are (this list will likely get too long to maintain eventually, but for now we can keep this up to date):
 * [`Nanoassembler`][reprap-wiki-nanoassembler] - the primary central information point on RepRap for attempting to develop true semiconductor manufacturing.
 * [`Nanoassembler Path - Diamond Direct-Doped Growth`][reprap-wiki-diamond-direct-growth] - Information point for the DDDG pathway. 

In some cases, a page will be worked on directly from this repository. In that case, there will be one `<page>.wiki` file (like normal), which attempts to track the version of the page on the RepRap wiki as closely as possible, and one extra file called `<page>.dev.wiki`, which contains the version to be worked on here.

Before editing the wiki page with some developments you've created, you should update `<page>.wiki` to the version directly from the latest edit on [reprap-wiki], compare that to your current `page.dev.wiki` version, and update your version with any relevant edits from the site version before posting the new version.

It might also be worth making git branches to update the wiki pages if you only intend to work on a page for a short period of time and otherwise it's not likely to need frequent updates. That way, you can avoid contaminating the main git branch with very stale `<page>.dev.wiki` file mirrors if they won't be updated much other than a few edits. For pages that are continually updated central resources on the RepRap wiki directly related to this project, you don't need to avoid creating `<page>.dev.wiki` files on the main branch. `Nanoassembler.dev.wiki` definitely falls into this category. 

### `paths`
This is the core directory containing most resources for the various semiconductor fabrication "paths". It's contents are organised hierarchically, with Markdown as the primary textual format. If you're working on some model (for anything not related to general-purpose device components like hyper-precise motors or air filtration or similar), citing specific scientific papers as part of documenting a specific potential component of a process with certain material types, or making general notes about a specific semiconductor process, this is where you want them. That's likely to be most things.

At the top level, `paths` refers to the concept described in the opening paragraph [here][rrwiki-semiconductor-paths]. This refers to the fuzzy "area" a potential set of technologies, processes, models, etc. are in. It's nowhere near as specific as a single semiconductor process, but it's not just "any tech goes". More accurately, it's a collection of related technologies (some which may do the same or similar things in different ways) associated with a common family of semiconductor substrate types (think Silicon, Diamond-ish, Plastic + Graphene, etc.). 

Each subdirectory ("semiconductor technology path") should have a fairly standard structure inside. Namely - once information or resources exist for each of these types of information - the following inner structure for that path should be made:
* A `diagrams` subdirectory (internally structured however is relevant). This is for diagrams to illustrate the proposed structure (e.g. curv files or rendered images from curv files, as well as associated notes), that aren't designed to function as actual construction information that could be sent to a hypothetical printer-like technology. 
* A `models` subdirectory (internally structured however is relevant). This is for models and structures (and notes) that could actually be sent to some machine (real or future) to be produced. If a model has an associated diagram, then that can be put inside a subdirectory of the specific model folder if so desired.
  * Filling this folder is likely to require progress in [modelling-repo]
* A `research` subdirectory - useful for storing research papers and notes on them. Be careful not to get trolled by the copyright of science journals - if something is behind a paid journal, it's better to add an empty file of the correct name with something DOI-like inside, so people can look up the paper themselves.
* General notes can go in the folder, and use the general markdown structure. If there's a relevant wiki page for the given subdirectory, you should probably reference it in the top-level document for that directory. 

### `general-purpose`
This is for models, notes, research, etc. that is independent of an individual semiconductor path - things like high-resolution motor systems or anti-contamination methods. However, if something is fairly strongly tied to a given path (e.g. the main way to manufacture it is to use a specific material or structure that is strongly tied to the production method of a specific semiconductor path), then it might be better to put it with other notes that are specific to that path. 

Subdirectories inside this should have a similar structure to that described [for paths](#paths).

### `lib`
This directory contains some library stuff for the creation of diagrams and models. For now, it's some extra `curv` stuff, but when we develop a modelling language designed specifically for this type of manufacturing and simulation, we might also put that here (though it could go directly in the `modelling` repository). 

[licensing]: https://gitlab.com/computing.hardware.autonomy/gitlab-profile#licensing

[lazy-union]: https://github.com/openscad/openscad/issues/350

[reprap-wiki]: https://reprap.org/wiki/

[curv]: https://codeberg.org/doug-moen/curv

[modelling-repo]: https://gitlab.com/computing.hardware.autonomy/modelling.git

[GFDL1.2]: http://www.gnu.org/licenses/old-licenses/fdl-1.2.html

[cc-by-sa-4.0]: http://creativecommons.org/licenses/by-sa/4.0/

[libfive]: https://github.com/libfive/libfive

[reprap-wiki-nanoassembler]: https://reprap.org/wiki/Nanoassembler
[reprap-wiki-diamond-direct-growth]: https://reprap.org/wiki/Nanoassembler_Path_-_Diamond_Direct-Doped_Growth 

[rrwiki-semiconductor-paths]: https://reprap.org/wiki/Nanoassembler#Developing_Technology_to_Actually_Do_This


## License
See [licensing] for information on how this project is licensed. It's a little complex because of wanting to use a hardware-specific and software-specific reciprocal/copyleft license, as well as needing compatibility with the RepRap wiki and it's common licensing schemes. 

[^future-modelling-repo-notes]: https://iquilezles.org/articles/
