{{Development:Stub}}
{{Development
|name=Nanoassembler
|title=Nanoassembler
|status=Concept
|description=Central page for researching construction methods for complex, multi-material structures with nanoscale patterns at a reasonable rate
|license=[[GPL]]
|author=Sapient cogbag
|contributors=[[User:Sapient cogbag]]
|categories=[[Category:Development]],[[Category:Nanoassembler]]
}}
This is the page for developing viable technology for comparatively rapid production of complex, multi-material devices with patterning and structure on the nanoscale at a reasonable rate. 
The most immediate goal is the production of full-capability semiconductor devices of at least a similar order-of-magnitude scale to that of modern semiconductor fabrication techniques, 
plus full 3D structure, though with significantly different processes of course.

== Sub-Page List ==
This is a simple list of the relevant top-level subpages for this project
* [[Nanoassembler]] (this page), and [[Category:Nanoassembler]]

=== Specific Technology Paths ===
* [[Nanoassembler Path - Diamond Direct-Doped Growth]] and [[:Category:Nanoassembler Path - Diamond Direct-Doped Growth]]

== Challenges (General) ==
Nanomanufacturing & Semiconductor Manufacturing poses very difficult challenges in a single-user-accessible or near-tabletop scale environment.

=== Construction Rate === 
Existing, top-down semiconductor manufacturing involves multiple very complex steps for producing even a single patterned layer. Each layer is only a few nanometres thick. 
The complex steps also take a long time and each require different environments, so the substrate has to be moved between multiple chambers for construction.

To make nanoassembly practical for home-ish users, the construction rate must be accelerated and have much simpler environmental requirements so the objects or devices being produced don't 
need to be moved around to different construction chambers for production. Furthermore, it must be possible to produce more "bulk" structures (without nanometre-scale patterning) at 
a very accelerated rate, as one of the major points of nanoassembly is the embedding of circuitry and other nanostructures within larger material. Ideally, construction rate would 
correspond roughly to the patterning complexity of the current layers.

An important aspect of this is that we can't continue current 3D printing techniques down to the nanoscale. Most 3D printers right now are based on a rasterisation like system - either a 
single mobile head (for deposition-like printers), or using crossed lasers point-by-point (like in resin printers). As the feature size of printers becomes smaller and smaller, these sorts 
of systems decrease in rate in a cubic fashion (unless the speed of "rasterising" a specific point increases sufficiently). If a current printer-style system takes several hours to print a 
30x30x30cm<sup>3</sup> region at 100micrometre resolution, then scaling each of those dimensions by 10,000 (for a 10nm resolution) would result in several hours for a 
3x3x3micrometre<sup>3</sup> region, which is ridiculously tiny, much smaller than human hair (which has width on the order of 50 micrometres).

=== Chemical Access and Handling ===
A lot of chemicals used in current silicon-based semiconductor manufacturing are very difficult or actively unsafe to handle. 

For example, most p-type doping is done using Borane gas, which is highly toxic. Any nanoassembly process designed for 3D-printing-like usage needs to use chemicals that are at least 
somewhat safe to handle by someone who's not got massive amounts of chemistry experience - any highly toxic reagants must be produced only ephemerally and only in trace amounts to be 
immediately consumed by the process, from safer raw materials.

This also applies to any kind of liquid-based process. Modern semiconductor manufacturing is moving away from liquid processes towards more gas, plasma, vacuum, or ion-based processes 
for a reason - liquids are very difficult to handle and keep pure and if we end up using them we need to ensure that it's only ever in microscopic amounts at a time, or that we always 
need the liquid rather than having to switch it out in the process, because that would likely be unacceptably slow and have the same issues as semiconductor manufacturers currently 
experience.

=== Ingredient Purity & Environmental Cleanliness ===
Much of modern semiconductor and nanoscale assembly requires extremely high purity of materials and extremely clean environments - even micron-scale contamination is unacceptable. For 
any nanomanufacturing system, this is likely to at least partially be the case, so building a tabletop nanoassembler requires embedding mechanisms to maintain and produce that kind of 
environment. Furthermore, the general expense of current equipment to make these sorts of environments means that it is of somewhat high importance that we can produce most of the parts 
needed for this from our system.

The cleanliness requirement also adds significant complexity to the construction of new nanoassemblers from parts produced by other ones. In particular, it's very desirable to figure out 
a system to encase the most sensitive parts (for example, a massively parallel, nanostructured toolhead) such that they can be inserted into a new machine without being exposed much to the 
environment. Furthermore, it's a good idea to be able to combine parts in a near-sealed fashion. That is, we need to figure out a way to combine certain produced parts with 
particularly sensitive surfaces while reducing or eliminating that sensitive surface's exposure to atmosphere, in a way that is still user friendly.

My proposal for this is producing parts inside enclosures which have structured weak points to allow slotting together, which then enables the inner parts to be slotted together, but we 
have a lot more work to do to design such a thing.

Ingredient purity is another problem. Modern semiconductor manufacture requires highly pure silicon substrate for best performance (though it can work with less pure substrate). Most 
important is a lack of contamination with things that affect the electronic band structure too much, and current semiconductor manufacturing processes require highly monocrystalline 
silicon as a prerequisite for the best possible results. For dopants, purity is only slightly less important (because you are already adding far less of that into the semiconductor and 
hence contaminants are even further rarified), but purity is still pretty important.

My suggestion for dopant purity is to include something like an electrostatics-based or even magnetic mass spectrometer to act as a filter. Importantly, it doesn't need to be super 
precise, just enough to filter out unwanted elements. However, it may also be acceptable to start by working with less pure ingredients as long as they are reasonably pure.

=== Masks ===
Current semiconductor processes are mostly oriented around production lines - and require the production and use of highly specialised masks, which are then used thousands of times to 
produce the patterning on a semiconductor's surface to then apply a single process to it in that pattern. This is then repeated in many different steps, using different processes and masks.

Masks are pretty much the antithesis of 3D printing and rapidly reconfigurable production systems. Any system we design must involve maskless processes and enable direct patterning without 
the intermediary production of another "thing" with that pattern on it. 

=== Massively Variable Processes ===
Modern semiconductor manufacturing involves many processes with extremely distinct environments and behaviour - some might require vacuum, some are additive, some are subtractive/etching (and 
in fact most mix additive and subtractive processes), some require liquids while some require large or small scale environments, etc. 

This is an absolute nightmare for the kind of nanoassembly machine that we may envision. Any kind of process that requires changes to the "bulk" environment (e.g. a whole chamber increasing 
or decreasing in temperature or pressure at rapid speed), rather than occurring ephemerally and locally (e.g. a laser heating one spot for a very short amount of time), is unacceptable if 
it's required for producing any *single* patterned material or a *single* layer, and takes more than a few seconds.

What we need is a *uniform* (or close to uniform) process to assemble more layers of distinct material, which is directly controllable. Having a large variety of processes doesn't work. 
Ideally, such a process would not require any kind of vaccuum or high temperature either.

=== Substrate Flatness ===
Building semiconductor chips usually starts with a layer of highly pure silicon of monocrystalline structure and near-atomically-smooth surface, which is pretty much required in order 
for the components built on and above that surface to actually connect with each other. Even micrometre-scale bumps - which may be almost invisible to us - are like a mountain compared 
to the sorts of nanometre scale features we want.

This means we need to design a process - and a somewhat-flat surface - that can be built upon or etched away (e.g. with lasers) to get something close to atomically flat. Furthermore, we 
need to construct our own substrate on top of this in a reasonably removable way, before our assembler can then start constructing nanometre-scale components on top.

=== Precision Assembly === 
Even once we devise a concrete method for combining nanoassembler components together by hand that avoids contaminating any nanostructured surfaces, we still need to deal with the issue of 
combining components with nanostructured surfaces to nanometre scale precision - or more specifically, I suspect trying to align components like that to be nearly impossible (though it may 
be doable with nanometre-scale fractal joints, if you can avoid them shattering). 

Instead, I strongly suggest that we prefer embedding methods of precisely measuring (electronically) the misalignments of components to nanometre scale and then using whatever our 
nanometre-precise positioning system is, and the way we transfer designs for construction, to compensate for these microscopic misalignments.

=== Precise Motion & Positioning ===
Another difficulty is precise positioning. Current nanometre scale positioning systems are expensive, inaccessible and focused on XYZ style, pointlike motion - though 
piezoelectric positioning does exist. I would strongly suggest that, once we are capable of reasonably small scale manufacture with semiconductors, we use this capability to 
build some kind of nanopositioned electrostatic motor system. To compensate for imprecise combination of positioning components, they should contain an embedded system to calculate 
their offset at each joint.

I have some ideas for that related to having essentially a very large number of adders and combining that with voltage offsets or calculated resistance due to nanoscale misalignment to 
derive accurate absolute positioning numbers, but haven't fleshed it out yet.

Another aspect of positioning is accounting for external vibrations that could disrupt any nanoscale structure. This may be accounted for with either active or passive damping - perhaps 
using electrostatics to tune the system to damp any common frequencies of vibration in the environment. 

Depending on material choices, we may need to account for temperature-based expansion - however, the production of doped semiconductor materials generally implies the ability to create 
Peltier-based solid state cooling devices (even if not super efficient), and temperature issues can hence be avoided by using such devices to keep any super-precise tools' macro-scale 
dimensions consistent by pumping out excess heat or pumping in heat if it gets too cold. 

=== Bootstrap Path ===
Something very desirable for any nanoassembly solution is being able to come up with a reasonably inexpensive bootstrap path from current printing and construction technology. Furthermore, 
it's pretty vital that - for feature sizes larger than a couple of nanometres - the system can construct components for assemblers of at least slightly higher precision than itself. 
This is to avoid a slow loss of precision and correctness as you start replicating and producing any nanostructured parts.

=== Modelling Software & Data Flow Rate ===
With any technology for serious creation of nanostructures, we need better modelling formats designed specifically for it. This is for a number of reasons:
* Existing systems like OpenSCAD - and the formats they transform to - do not have the most robust and standardised representation of material properties (such as doping levels). OpenSCAD in particular lacks full colour output and more general solutions for specific properties (e.g. dopant levels, material, etc.) to be encoded in constructive solid geometry are desirable, especially for simulation of functional components
* Other existing systems that do support volumetric information (e.g. curv) - often do it in a colour-only way as they are intended mostly for art rather than material properties. Though OpenVCAD may provide a partial solution here. We use curv for the functional diagrams here but we will need a better system for modelling and simulation with first-class support for material properties directly
* The sheer quantity of information requires better formats for massively parallel construction instructions. A machine with 10nm by 10nm construction mechanisms in a 10cm x 10cm construction array has 10<sup>7</sup> cells on each axis, or 100 trillion (10<sup>14</sup>) cells of data. For a reasonable construction rate, each of these must be controlled 1000s of times per second (at the highest level of detail). If we assume 1 byte per control sequence per cell, that's a maximum data rate of 100 petabytes/second. Sending that sort of data is completely ridiculous. As such, the systems we design need some kind of layered system with decomposable, parameterisable abstractions that get split up recursively, only turned into instructions at the bottom layer. Bear in mind - current top-end GPUs can do about 1 petaflop/s at 8 bit precision.

As such, we need better FOSS programs, capable of producing a format that doesn't encode much raw instructions, but actual parameterised information, otherwise the data flow becomes absurd. G-code just ain't gonna cut it for this :p 

== Developing Technology to Actually Do This ==
This is obviously a very daunting task. Progress, however, can and has been made.

For ease of terminology and referring to relevant technologies, I propose the notion of a *pathway* for integrated circuit and semiconductor manufacturing, which refers to a somewhat 
fuzzy collection of things like the substrate to use and how it can be constructed, what sort of dopants it might involve (and the type of techniques involved in created doped 
semiconductor material), the kinds of chemicals and environments that need to be made, etc.

=== Traditional Silicon Path ===
The "Traditional Silicon Path" is what I would call the technology path most focused on replicating existing semiconductor manufacturing but in a tabletop/3D-printer-like environment. 
This would primarily focus on using, for example, highly localised environmental changes for things like etching, rather than liquid/wet processes, as well as some mechanism to dope the 
silicon using localised high temperatures rather than turning everything into a furnace.

However, I think this is unlikely to be a successful path. In particular, without masks and without photoresist (which has the same issues as any wet process), the primary way 
to get resolution anywhere close to modern chips is via direct electron-induced sputter deposition and sputter doping, or ion beam doping. Both of these have pretty big problems. 
Furthermore, the process doesn't allow for growing the bulk silicon (even atomically thin silicon layers require the spontaneously flammable and toxic gas Silane (SiH4)), so the 
hypothetical assembler would need an entirely different toolset and process for that too.

=== Plastic-Graphene Path ===
One area that has been somewhat explored is that certain types of plastic - like Kepton, which can likely be produced in thin film layers by using the two soluble reagents that are used 
to create it plus an atomiser - turn into graphene when hit with specific amounts or types of light.

I've explored this path to some degree, and it does make a fairly decent potential process to sit alongside current 3D printing technology due to the ease of handling the 
substrate reagents for creating thin-film Kepton layers. 

However, I have concerns about doping (though laser doping presents a potential pathway here, but the use of optics severely restricts minimum feature size and it makes doping a 
secondary non-additive process), and it is once again a secondary process on top of existing 3D printing tech, though one much more compatible with it than any hypothetical Silicon 
process would be. I may post some of my work on this, and people who've made more progress on this front I'd encourage you to add it :)

=== Diamond Direct-Doped Growth ===  
The path I've mostly been working on lately. This is a path based on growing diamond-like material, with the precursor gases pre-mixed with dopant in external chambers for direct deposition of doped material. 

The main page with information on this path is [[Nanoassembler Path - Diamond Direct-Doped Growth]], and it has a [[Category:Nanoassembler Path - Diamond Direct-Doped Growth]] for pages related to that.

There are various processes that I've researched that provide potential routes to do chemical vapour deposition of diamond without intense heat/furnace stuff (mostly, plasma and electric field related things), and generalised directed deposition of material via CVD has been demonstrated if by a slightly different mechanism than my various ideas.

This path seems by far the most useful and viable to me, so most of my work will be here.

A lot of the processes also permit accelerated growth of diamond when you care less about fine-grained features, which means it is probably possible to build semi-bulk structures with the same process components as the fine-grained structures. 

The bulk ingredients and feedstock are chemicals like methane (which we could even use electrocatalysis to synthesise from CO2 and water if we discover a process for that), and growth of diamond with boron doping has been demonstrated with the relatively non-toxic boron oxide (though we'd likely use an intermediate mixing process to create small amounts of feedstock gas with different dopant concentrations). 

{{dl}}
