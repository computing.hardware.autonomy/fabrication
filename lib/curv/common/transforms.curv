// Utility file containing tranforms and other combinators. 
let
    maths = file "maths.curv"; 
in {
    /// Take an existing shape and rotate it such that the positive z axis is now aligned 
    /// with the provided axis instead. 
    /// 
    /// This will modify the bounding box to accommodate the rotation. Repeated applications 
    /// may cause unlimited bounding box expansion nya
    reorient_z (new_axis::is_vec3) (shape::is_shape) = let 
        // this transform is *to* the appropriate z-axis coordinate system.
        // and inverse takes things from that z-axis coordinate system to 
        // the main coordinate system nya
        { transform, inverse } = (maths.xynormal_coordinate_transform new_axis); 
        new_bbox = maths.bbox_matrix_transform inverse (shape.bbox);
    in make_shape {
        is_2d = shape.is_2d;
        is_3d = shape.is_3d;
        bbox = new_bbox;
        dist p = let 
            // curv doesn't like expansion via ... in GLSL code. use indexing instead.
            in_orig_coords = dot [p.[[X, Y, Z]], transform];
        in shape.dist [in_orig_coords.[X], in_orig_coords.[Y], in_orig_coords.[Z], p.[T]];
        colour p = let 
            in_orig_coords = dot [p.[[X, Y, Z]], transform]; 
        in shape.colour [in_orig_coords.[X], in_orig_coords.[Y], in_orig_coords.[Z], p.[T]]; 
    };
    
    /// exponential-smoothing union. 
    /// See: https://geti2p.net/en/docs/tunnels/implementation#ordering
    /// This exploits the commutativity of exponential smoothmin to allow arbitrary number of union. 
    /// However, for various reasons, we can't add weighting or anything here in addition to the smin. 
    /// 
    /// A default sensible value for `k` is 32. Note - we also use `e` as a base rather than 2, and `ln`
    /// rather than `log2`, as these are the functions available in curv nya
    expsmooth k = {
        /// Maximum expansion in every direction in every axis of the exponential-smoothing process for 
        /// the given # of shapes nya
        max_expansion (shape_count::is_num) = (log shape_count)/k; 

        /// Trying to make lists of shapes work directly with curv's GPU code system doesn't seem to work 
        /// very well without recursive/"fold"-like construction nya. We found this the hard way in the 
        /// `make_joint` function. So instead, we borrow the technique from the stdlib of making a
        /// binary union function and folding over it ^.^
        /// 
        /// This does the same colour prioritisation mechanism as stdlib union and smooth union, rather than 
        /// trying to blend the colours exponentially. That should be doable in future, but for now I just want
        /// a working implementation and adding that into recursion is a bit on the complex side nya. 
        ///
        /// Note that you shouldn't use `_union2` directly. It has an incorrect bbox calculation - deliberately
        /// so, because the exponential smoothmin function has a smaller "maximum expansion" if you merge 
        /// the calculations all in one rather than do it for each pair. In particular, the maximum expansion
        /// for exponential smoothmin over `n` shapes is `(ln n)/k` - see: https://en.wikipedia.org/wiki/LogSumExp
        /// but if you were to do it pair by pair, the accumulated result would be ((n - 1) * (ln 2)/k), which grows
        /// linearly rather than logarithmically in the number of shapes nya.
        /// The bbox of this is, then, the bbox-union of the bboxes of the inner shapes. This is used as an intermediary
        /// result in the final union.
        _union2 [s1, s2] = make_shape {
            dist p: maths.exponential_smin [k, [s1.dist p, s2.dist p]],
            colour p: let 
                    d1 = s1.dist p;
                    d2 = s2.dist p;
                // pick d2 if inside d2, or inside d2 and d1 but d2 is closer, else pick d1 nya
                in 
                    if (d2 <= 0 || d2 <= d1) s2.colour p else s1.colour p,
            bbox: [
                // This is taken from the same component in the curv stdlib
                // min seems to broadcast ok nya. 
                min [s1.bbox.[MIN], s2.bbox.[MIN]],
                max [s1.bbox.[MAX], s2.bbox.[MAX]]
            ],
            is_2d: s1.is_2d && s2.is_2d,
            is_3d: s1.is_3d && s2.is_3d,
        };

        union (shapes::is_list) = let 
            expansion = if ((count shapes) == 0) 0 else max_expansion (count shapes); 
            apply_expansion shape_with_raw_bbox = let
                new_bbox = [
                    shape_with_raw_bbox.bbox.[MIN] - expansion,
                    shape_with_raw_bbox.bbox.[MAX] + expansion, 
                ];
                in set_bbox new_bbox shape_with_raw_bbox;
            res = reduce [nothing, _union2] shapes; 
        in res >> apply_expansion;
    } 
}
// License: GPLv3+
